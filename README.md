<!--
SPDX-FileCopyrightText: 2021-2023 Robin Vobruba <hoijui.quaero@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

# OSH related file-type meta-data

[![GitHub license](
    https://img.shields.io/gitlab/license/OSEGermany/osh-file-types.svg?style=flat)](
    LICENSE.txt)
[![REUSE status](
    https://api.reuse.software/badge/gitlab.com/OSEGermany/osh-file-types)](
    https://api.reuse.software/info/gitlab.com/OSEGermany/osh-file-types)

Lists file formats by their typical file extension and related meta-data,
separated into categories, one category per CSV
([Comma-separated values](
https://en.wikipedia.org/wiki/Comma-separated_values))
file:

* [CAD](res/data/cad.csv)
  ([Computer Aided Design](https://en.wikipedia.org/wiki/Computer-aided_design))
  * formats suitable to describe 3D mechanical designs
  * [CAD example image](https://upload.wikimedia.org/wikipedia/commons/8/89/Schneckengetriebe.png)
* [PCB](res/data/pcb.csv)
  ([Printed circuit board](https://en.wikipedia.org/wiki/Printed_circuit_board))
  * formats suitable to describe schematics of electronics and  PCBs
  * [PCB example image](https://upload.wikimedia.org/wikipedia/commons/a/a4/SEG_DVD_430_-_Printed_circuit_board-4276.jpg)
  * [Schematics example image](https://upload.wikimedia.org/wikipedia/commons/7/76/4_bit_counter.svg)

Each file contains the following columns.
A more machine-readable version can be found in
[format_definition.csv](format_definition.csv):

| Name | Valid Data | Example |
| --- | ------ | --- |
| File extension | `"str"` | sch |
| File format | `open|proprietary|unknown` | open |
| Encoding | `text|binary|both|unknown` | text |
| Category | `source|export` | source |
| Name | `"str"` | KiCad Schematics |
